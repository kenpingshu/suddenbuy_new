<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Controller {
        
        public $account = ''; 
        public $pwd = '';
    
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
//		$this->load->view('welcome_message');
	}
        
        public function login($account='',$pwd='')
	{
            
            $account = $this->security->xss_clean($account);
            $pwd = $this->security->xss_clean($pwd);
            $pwdmd5 = md5($pwd);

//            var_dump($account);
//            var_dump($pwd);
//            var_dump($pwdmd5);
            
            $sql = "SELECT * FROM member WHERE account = ? AND pwd = ? ";
            $query = $this->db->query($sql, array($account , $pwdmd5));
//            var_dump($query->result());
            
            if ($query->num_rows() === 1 )
            {
                    foreach ($query->result() as $row)
                    {
//                        $this->load->library('session');
                        $this->session->set_userdata('id', $row->id);
                        $this->session->set_userdata('account', $row->account);
                        $this->session->set_userdata('pwd', $row->pwd);
//                        var_dump($_SESSION);
                        echo '1';
                        return;
                    }
            }  else {
                echo '0';
                return;
            }
            
	}
        
        public function checklogin()
	{
//            $this->load->library('session');
            
            if($this->session->has_userdata('account') && $this->session->has_userdata('pwd') && $this->session->has_userdata('id')){
                echo '1';
                return;
            }else{
                echo '0';
                return;
            }
	}
        
        public function logout()
	{
//            $this->load->library('session');
            $this->session->unset_userdata('id');
            $this->session->unset_userdata('account');
            $this->session->unset_userdata('pwd');
            return;
	}
}
