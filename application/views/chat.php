<script src="https://cdn.socket.io/socket.io-1.2.0.js"></script>
<script src="http://code.jquery.com/jquery-1.11.1.js"></script>
<script>
var socket = io('http://localhost:3333');//應該是因為有載入socket.io 可直接這樣實例化一個io物件
$(document).ready(function(){
/*
    $(".talk").click(function(){
        $("#chatroom").show();
    });*/

    //tell server
    socket.emit("add user","<?php echo $account; ?>");//在使用者輸入名稱之後 透過socket.emit傳送訊息給server 觸發socket.on('add user',...)事件

    //監聽新訊息事件
    socket.on('chat message', function(data){//新增一個監聽事件 名稱為chat message 等待server觸發並代入訊息
      appendMessage(data.username,data.msg);
    });

    $("#m").keydown(function(event){
      if ( event.which == 13 ){
        var text = $('#m').val();
        socket.emit('chat message', text);//在輸入聊天訊息後 透過socket.emit觸發server的socket.on('chat message',...)事件
        $('#m').val('');
        return false;
      }
    });

    function appendMessage(username,msg){

      if(username == '<?php echo $account; ?>'){
        $message_class = "self";
      }else{
        $message_class = "other";
      }
      var message = '<div class="'+$message_class+'">'+
      '<div class="id-name">'+username+'：</div>'+
      '<div class="cha-word">'+msg+'</div>'+
      '</div>';
      $('#cha-log').append(message);
      var cha_log = document.getElementById("cha-log");
      cha_log.scrollTop = cha_log.scrollHeight;
    }

    $("#close").click(function(){
        $("#chatroom").hide();
    });

    $("#small").click(function(){
        $("#chatroom").removeClass("chatroom").addClass("chatroom-close");
        $("#small").hide();
        $("#cha-log").hide();
        $("#m").hide();
    });

  });
</script>
<div id="chatroom" class="chatroom">
	<div class="cha-title"><?php echo $account; ?>
    	<img id="close" src="img/cha-close.png" style="cursor:pointer;"/>
      <img id="small" src="img/cha-small.png" style="cursor:pointer;"/>
  </div>
  <div id="cha-log" class="cha-log">
  </div>
  <input id="m" type="text" class="txt" value="輸入訊息..." onfocus="cleartext(this)" onblur="resettext(this)"/>
</div>