<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" href="css/style.css" />
        <script type="text/javascript" src="js/jquery-2.0.2.min.js"></script>
        <title>無標題文件</title>
        <style>
            html, body {
                height: 100%;
                margin: 0;
                padding: 0;
            }
            #map {
                height: 100%;
            }

        </style>
    </head>

    <body>
        <div id="cover"></div>
        <div class="wallpaper">
            <div id="map"></div>
        </div>
        <div class="topbar">
            <img src="img/logo-word.png" />
            <?php if(empty($_SESSION['id']) || empty($_SESSION['account']) || empty($_SESSION['pwd'])){ ?>
            <a href="#" id="notice"><img src="img/list-icon.png" /></a>
            <?php }else{ ?>
            <a href="javascript:void(0)" onclick="logout()" id="notice_out">登出</a>
            <?php } ?>
        </div>
        <?php require_once(APPPATH . "views/chat.php"); ?>
        <?php require_once(APPPATH . "views/login.php"); ?>
        <?php require_once(APPPATH . "views/task.php"); ?>
        <?php require_once(APPPATH . "views/checkTask.php"); ?>
        <script>

            var missionList = <?php echo json_encode($missionList) ?>;


            function initMap() {
                var map = new google.maps.Map(document.getElementById('map'), {
                    center: {lat: -34.397, lng: 150.644},
                    zoom: 15
                });
                var infoWindow = new google.maps.InfoWindow({map: map});

                // Try HTML5 geolocation.
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function (position) {
                        var pos = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        };

                        infoWindow.setPosition(pos);
                        infoWindow.setContent('Location found.');
                        map.setCenter(pos);
                    }, function () {
                        handleLocationError(true, infoWindow, map.getCenter());
                    });
                } else {
                    // Browser doesn't support Geolocation
                    handleLocationError(false, infoWindow, map.getCenter());
                }

                map.addListener('click', function (e) {
                    placeMarkerAndPanTo(e.latLng, map);
                });
//                var markers = [];
//                for ( var index in missionList )
//                {				
//                        //設定各查詢位址的標記Marker
//                        markers[index] = new google.maps.Marker({		  
//                          position: new google.maps.LatLng( parseFloat(missionList[index].lat) ,parseFloat(missionList[index].lng)),
////                          title:MarkAry[index][0],
//                          map:map		//要顯示標記的地圖
//                        });	
//                        //設定 各標記點Marker的click事件		
//                        google.maps.event.addListener(markers[index], 'click', function() {
//                                 if (infoWindow){ infoWindow.close();}
//                                infoWindow.setContent("<div>" + missionList[index].require_desc + "</div>");
//                                infoWindow.open(map,markers[index]);
////                                ShowInfo(map , this, "<div>" + missionList[index].require_desc + "</div>", infoWindow);			
//                        });
//
//                }



                var markers = [];
                var infowindow2 = new google.maps.InfoWindow();
                for (var k in missionList) {
//                    console.log(missionList[k]);
                    var myLatLng = {lat: parseFloat(missionList[k].lat), lng: parseFloat(missionList[k].lng)};

                    markers[k] = new google.maps.Marker({
                        position: myLatLng,
                        title: missionList[k].require_desc,
                        mission: missionList[k],
                        map: map
                    });
                }

                for (var k in markers) {
//                    console.log(missionList[k].require_desc);
                    google.maps.event.addListener(markers[k], 'mouseover', function () {
                        console.log(content);
                        infowindow2.setContent("<div>" + this.title + "</div>");

                        infowindow2.open(map, this);
                    });
                    google.maps.event.addListener(markers[k], 'mouseout', function () {
                        infowindow2.close();
                    });

                    google.maps.event.addListener(markers[k], 'click', function () {
                        console.log(this.mission);
                        showTask(this.mission);
                    });
                }


            }

            function showTask(mission) {
                $("#c_id").text(mission.member_id);
                $("#c_address").text(mission.lat +","+ mission.lng);
                $("#c_req").text(mission.require_desc);
                $("#c_price").text(mission.price_start);
                $("#c_type").text(mission.transaction_type);
                $("#c_vd").text(mission.valid_date_start +""+ mission.valid_date_end);
                $("#accept").attr("href","mission/update_receive/<?php echo $this->session->userdata('id')?>/"+mission.id);
                
                $('#cover').css('opacity', 0);
                //  $('body').css('overflow-y', 'hidden');
                $("#cover").delay(0).animate({
                    opacity: .8
                }, 0);
                $("#cover").show();
                $('#task-chack').css('opacity', 0);
                $("#task-chack").delay(200).animate({
                    top: '10%',
                    opacity: 0
                }, 0).delay(50).animate({
                    top: '15%',
                    opacity: 1
                }, 500);
                $("#task-chack").show();
                return false;
            }

            function ShowInfo(mapObj, markerObj, content, infowindow)
            {//開啟資訊視窗		
//                var infowindow = new google.maps.InfoWindow();  
                ;
            }

            function handleLocationError(browserHasGeolocation, infoWindow, pos) {
                infoWindow.setPosition(pos);
                infoWindow.setContent(browserHasGeolocation ?
                        'Error: The Geolocation service failed.' :
                        'Error: Your browser doesn\'t support geolocation.');
            }

            function placeMarkerAndPanTo(latLng, map) {
                var marker = new google.maps.Marker({
                    position: latLng,
                    map: map
                });
                map.panTo(latLng);
                var myLatlng = {};
                myLatlng.lat = latLng.lat();
                myLatlng.lng = latLng.lng();
<?php if ($isLogin) { ?>
                    showMissionBlock(myLatlng);
<?php } else { ?>
                    showLogin();
<?php } ?>
            }


////////////////////
            function resettext(id) {
                //恢復文字
                if (id.value == "")
                {
                    id.value = id.defaultValue;
                    id.className = "txt";
                }
            }
            function cleartext(id) {
                //清除文字
                id.value = "";
                d.className = "";
            }
            $("#notice").click(function () {
                showLogin();

            });
            $("#close_btn, #cover").click(function () {
                // 		  $('body').css('overflow-y', 'auto');
                $("#login, #pop_race").hide();
                $("#cover").hide();
                return false;
            });

            function showLogin() {
                $('#cover').css('opacity', 0);
                //  $('body').css('overflow-y', 'hidden');
                $("#cover").delay(0).animate({
                    opacity: .8
                }, 0);
                $("#cover").show();
                $('#login').css('opacity', 0);
                $("#login").delay(200).animate({
                    top: '10%',
                    opacity: 0
                }, 0).delay(50).animate({
                    top: '15%',
                    opacity: 1
                }, 500);
                $("#login").show();
                return false;
            }
            function login() {
                var id = $('#ID').val();
                var pwd = $('#PASSWORD').val();
//                console.log('./Member/login/' + id + '/' + pwd);
                $.ajax({
                    url: './Member/login/' + id + '/' + pwd,
                    type: "GET",
                    dataType: 'text',
                    success: function (msg) {
                        if (msg == '1') {
                            alert("登入成功！");
                            $("#login").hide();
                            window.location.href = 'http://dev.suddenbuy.com';
                        } else {
                            alert("登入失敗！")
                        }
                    }
                });
            }

            function logout() {
                $.ajax({
                    url: './Member/logout',
                    type: "GET",
                    dataType: 'text',
                    success: function (msg) {
                        window.location.href = 'http://dev.suddenbuy.com';
                    }
                });
            }


            function showMissionBlock(myLatlng) {
                $("input[name='lat']").val(myLatlng.lat);
                $("input[name='lng']").val(myLatlng.lng);
                $("#address").val(myLatlng.lat + "," + myLatlng.lng);

                $('#cover').css('opacity', 0);
                //  $('body').css('overflow-y', 'hidden');
                $("#cover").delay(0).animate({
                    opacity: .8
                }, 0);
                $("#cover").show();
                $('#task').css('opacity', 0);
                $("#task").delay(200).animate({
                    top: '10%',
                    opacity: 0
                }, 0).delay(50).animate({
                    top: '15%',
                    opacity: 1
                }, 500);
                $("#task").show();
                return false;
            }

            $("#close_btn, #cover").click(function () {
                // 		  $('body').css('overflow-y', 'auto');
                $("#task, #pop_race, #task-chack").hide();
                $("#cover").hide();
                return false;
            });
            
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAxaIW49oBhJCAh81XCzSX-_iIql3oJGIw&sensor=true&signed_in=true&callback=initMap"
                async defer>
        </script>
    </body>
</html>
