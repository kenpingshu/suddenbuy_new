<?php
class Mission_model extends CI_Model {

        //public $title;
        //public $content;
        //public $date;

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

        public function get_mission_total()
        {
                $query = $this->db->get('mission');
                return $query->result();
        }
        public function get_mission_lat($member_id="")
        {
            if($member_id!=''){
                $query = $this->db->query("SELECT * FROM mission where member_id='$member_id' and accept_member_id='' and mission_status=''");
            }else{
                $query = $this->db->query("SELECT * FROM mission where mission_status='' and accept_member_id=''");
            }
                            
                $arr_json=array();
                foreach ($query->result() as $row)
                {
                    $arr_display=array(
                        'id'=>$row->id,
                        'lat'=>$row->lat,
                        'lng'=>$row->lng,
                        'require_desc'=>$row->require_desc,
                        'price_start'=>$row->price_start,
                        'transaction_type'=>$row->transaction_type,
                        'valid_date_start'=>$row->valid_date_start,
                        'valid_date_end'=>$row->valid_date_end,
                        'member_id'=>$row->member_id,
                        'accept_member_id'=>$row->accept_member_id,
                        'mission_status'=>$row->mission_status,
                    );
                   array_push($arr_json, $arr_display);
                }
                return $arr_json;
        }
        public function get_mission_member($member_id)
        {
            
                $query = $this->db->get('mission', $this, array('member_id' => $member_id));
                $arr_json=array();
                foreach ($query->result() as $row)
                {
                   array_push($arr_json, $row->address);
                }
                return $query->result();
        }

        public function insert_mission()
        {
                $this->lat    = $_POST['lat']; // please read the below note
                $this->lng    = $_POST['lng']; // please read the below note
                $this->require_desc  = $_POST['require_desc'];
                $this->price_start  = $_POST['price_start'];
                $this->price_end  = 0;
                $this->transaction_type  = $_POST['transaction_type'];
                $this->valid_date_start  = $_POST['valid_date_start'];
                $this->valid_date_end  = $_POST['valid_date_end'];
                $this->member_id  = $_POST['member_id'];
                $this->date_create  = date('Y-m-d H:i:s');
                $this->accept_member_id  = '';
                $this->mission_status  = '';

                $this->db->insert('mission', $this);
                return 1;
        }

        public function update_mission($mission_id)
        {
                $query = $this->db->query("UPDATE `mission` SET `mission_status` = '1' WHERE `id` = '$mission_id'");
                return 1;
        }
        public function update_mission_receive($accept_member,$mission_id)
        {
                $query = $this->db->query("UPDATE `mission` SET `accept_member_id` = '$accept_member', `mission_status` = '0' WHERE `id` = '$mission_id'");
                return 1;
        }

}
?>