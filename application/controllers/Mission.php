<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mission extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
                $data['title'] = 'SuddenBuy';
//                $this->load->library('jquery');
                $this->load->helper('form');
		$this->load->view('mission/index');
	}
        public function create(){//建立任務
            $this->load->helper('url');
            $this->load->model('mission_model');
            $this->mission_model->insert_mission();
            echo "<script language=\"javascript\">";
            echo "window.location.href = '".base_url()."';";
            echo "</script>";
        }
        public function get_mission(){//取得任務
            $this->load->model('mission_model');
            $arr_get_mission = $this->mission_model->get_mission_total();
            return $arr_get_mission;
        }
        public function get_mission_lat(){//取得任務
            $member_id=$this->input->get('member_id');
            $this->load->model('mission_model');
            return  $this->mission_model->get_mission_lat($member_id);
        }
        public function update_status(){
            $mission_id=$this->input->get('mission_id');
            $this->load->model('mission_model');
            return  $this->mission_model->update_mission($mission_id);
        }
        public function update_receive($member_id,$mission_id){
            $this->load->helper('url');
            $this->load->model('mission_model');
            $this->mission_model->update_mission_receive($member_id,$mission_id);
            echo "<script language=\"javascript\">";
            echo "window.location.href = '".base_url()."';";
            echo "</script>";
        }
}
